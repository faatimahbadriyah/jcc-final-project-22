# Project Akhir

## Kelompok 22

### Anggota Kelompok

<ul>
    <li>Muhammad Faisal Abduh (muzhafr2@gmail.com) </li>
    <li>Dina Fauziyah (dfauziyah18@gmail.com)</li>
    <li>Fatimah Ulwiyatul (faatimahbadriyah@gmail.com) </li>
</ul>

## Tema Project

Aplikasi Booking Futsal

<br>

# ERD

![](public/img/ERD/ERD-Booking-Futsal.png)

<br>

# Link Video

Link Demo Aplikasi: https://drive.google.com/file/d/1dv1P6bVB3nstYAmPxD3KCh-5e3G_2Pgj/view?usp=sharing

Link Deploy: http://rental22.herokuapp.com

<br>

# Pembagian Tugas
Fatimah:
1. Membuat file migrasi transaksi dan user
2. Autentikasi laravel dan middleware role
3. Membuat seeder lapangan, user dan jadwal
4. CRUD transaksi
5. Upload bukti bayar 
6. Download invoice dengan plugin dompdf
7. Verifikasi Pembayaran 
8. Setup email notifikasi transaksi
9. Update tema dashboard
10. Upload photo profile

Faisal:
1. Membuat migrasi profil
2. Membuat CRUD profil - user
3. Membuat halaman depan 
4. Web-host testing
5. Demo video testing localhost dan webhost

Dina:
1. Membuat migrasi lapangan & jadwal
2. Membuat CRUD lapangan & jadwal
3. Membuat styling CSS utk appearance semua halaman (plugin toastr & datatables)
4. Web host testing
5. Membuat ERD dan migrasi tabel dasar
6. Test profil admin untuk approve bukti pembayaran