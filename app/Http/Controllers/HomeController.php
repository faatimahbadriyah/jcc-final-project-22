<?php

namespace App\Http\Controllers;

use App\Jadwal;
use App\Lapangan;
use App\User;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [
            'user' => User::count(),
            'lapangan' => Lapangan::count(),
            'jadwal' => Jadwal::count(),
        ];
        return view('index', compact('data'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {
        $lapangan = Lapangan::with(['jadwal' => function ($q) {
            $q->where('status', 'available');
        }])->get();
        return view('home', compact('lapangan'));
    }
}