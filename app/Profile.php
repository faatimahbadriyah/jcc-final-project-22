<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "profiles";
    protected $fillable = ["fullname", "gender", "address", "phone", "photo", "user_id"]; // whitelist

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}