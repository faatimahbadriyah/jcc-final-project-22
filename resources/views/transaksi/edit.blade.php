@extends('layouts.template')

@section('title')
Ubah Data Pesanan
@endsection

@section('content')
<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">Lengkapi form berikut.</h3>
    </div>

    <form action="/transaksi/{{$transaksi->id}}" method="POST">
        <!-- /.card-header -->
        <div class="card-body">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label class="col-form-label" for="nama_tim">NAMA TIM</label>
                <input type="text" class="form-control" name="nama_tim" id="nama_tim" placeholder="Enter ..."
                    value="{{$transaksi->nama_tim}}" required>
                <input type="hidden" value="{{$transaksi->jadwal_id}}" name="old_jadwal">
            </div>
            <div class="row">
                @forelse($lapangan as $lap)
                <div class="col-md-3 col-sm-6 col-xs-6 col-6">
                    <h6><strong>{{$lap->name}}</strong></h6>
                    @forelse($lap->jadwal as $j)
                    <!-- <div class="form-check">
                        <input class="form-check-input" type="radio" name="jadwal_id" value="{{$j->id}}" required>
                        <label class="form-check-label">{{$j->jam}} --- <strong>Rp.{{$j->harga}}</strong></label>
                    </div> -->
                    <div class="info-block block-info">
                        <div class="btn-group" >
                            <label class="btn btn-secondary">
                                <div class="bizcontent">
                                    <input class="radio-custom" type="radio" name="jadwal_id" value="{{$j->id}}" style="position:absolute; right:0; top:0" autocomplete="off" required>
                                    <div class="row">
                                        <div class="col-12">
                                            <span class="text-sm font-weight-normal">{{$j->jam}}</span>
                                            <h3><strong>Rp.{{$j->harga}}</strong></h3>
                                        </div>
                                    </div>
                                </div>
                            </label>
                        </div>
                    </div>
                    @empty
                    <p>Penuh</p>
                    @endforelse
                </div>
                @empty
                <p>Tidak ada data lapangan</p>
                @endforelse
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            @if(!empty($lapangan))
            <button type="submit" class="btn btn-primary">Submit</button>
            @endif
            <a href="{{url('transaksi')}}" class="btn btn-default float-right">Cancel</a>
        </div>
    </form>
</div>
@endsection
