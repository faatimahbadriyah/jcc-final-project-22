<footer class="main-footer navbar-dark text-white">
    <span>Copyright &copy; 2021 <a href="https://adminlte.io">Kelompok 22</a>.</span>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 3.1.0
    </div>
</footer>